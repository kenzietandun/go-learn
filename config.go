package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"syscall"

	"golang.org/x/crypto/ssh/terminal"
	"gopkg.in/ini.v1"
)

func getCredentials(configFile string) (string, string, error) {
	cfg, err := ini.Load(configFile)
	if err != nil {
		return "", "", err
	}

	username := cfg.Section("credentials").Key("user").String()
	password := cfg.Section("credentials").Key("pass").String()

	if username == "" || password == "" {
		return "", "", errors.New("Username/password not specified in config file")
	}
	return username, password, nil
}

func getUserInputCreds() (string, string) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Your UC username: ")
	username, _ := reader.ReadString('\n')

	fmt.Println()
	fmt.Println("Your password won't be shown on screen for security purposes")
	fmt.Print("Password: ")
	bytePassword, _ := terminal.ReadPassword(int(syscall.Stdin))
	password := string(bytePassword)
	fmt.Println()

	return username, password
}

func getDownloadDir(configFile string) (string, error) {
	cfg, err := ini.Load(configFile)
	if err != nil {
		return "", err
	}

	downloadDir := cfg.Section("download").Key("dir").String()

	if downloadDir == "" {
		return "", errors.New("Target directory not specified in config file")
	}

	return downloadDir, nil
}

func testLogin(username string, password string) error {
	initialCookie, err := loginLearn(loginURL, username, password)
	moodleCookie = initialCookie
	if err != nil {
		return err
	}

	return nil
}

func setupConfigFile(configFile string) error {
	if _, err := os.Stat(configFile); err == nil {
		return nil
	}

	fmt.Println("Creating config directory")
	err := os.MkdirAll(filepath.Dir(configFile), 0755)
	if err != nil {
		return err
	}

	username, password := getUserInputCreds()

	fmt.Println("Generating configuration file")

	f, err := os.Create(configFile)
	defer f.Close()
	if err != nil {
		return err
	}

	_, err = f.WriteString(`[credentials]
# UC username
user = ` + username + `
# Password
pass = ` + password + `

# Put where should all the files be stored in
# For Windows users, You want something like \Users\<your username>\go-learn\
# For Mac users, /Users/<username>/go-learn/
# For Linux users, /home/<username>/go-learn/
[download]
# e.g.
# dir = \Users\john smith\mysecretfolder 5\unifiles\
dir = ` + home + `/go-learn/`)

	if err != nil {
		return err
	}

	err = os.Chmod(configFile, 0600)
	if err != nil {
		return err
	}

	fmt.Printf("...done!\n")
	return err
}
