package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
	"github.com/mitchellh/go-homedir"
)

var (
	VERSION    = "0.68"
	home, _    = homedir.Dir()
	configFile = filepath.Join(home, "go-learn/learn.ini")
	dbFile     = filepath.Join(home, "go-learn/learn.sqlite")
	loginURL   = "https://learn.canterbury.ac.nz/login/index.php#login"
	ezproxyURL = "https://login.ezproxy.canterbury.ac.nz/login"
	rootURL    = "https://learn.canterbury.ac.nz/"

	moodleCookie  = ""         // initial cookie from Learn, used for logging in
	learnCookie   = ""         // after logging in to Learn use this cookie for future requests
	ezproxyCookie = ""         // cookie for downloading past papers
	client        *http.Client // global http client
)

func main() {
	PrintASCII()
	fmt.Println("ver.", VERSION)

	coursePtr := flag.String("course", "all", "course")
	flag.Parse()

	cYel := color.New(color.FgYellow)
	cRed := color.New(color.FgRed)

	HTTPClient, err := setupHTTPClient()
	checkErr(err)

	client = HTTPClient // set global client

	// login error, dont write config file
	err = setupConfigFile(configFile)
	if err != nil {
		cRed.Println(`
[ERROR] Unable to setup configuration file,
please check if your username/password is correct
and try running the program again.`)
		waitInputExit()
	}

	db := initDatabase(dbFile)

	username, password, err := getCredentials(configFile)
	checkErr(err)

	downloadDir, err := getDownloadDir(configFile)
	checkErr(err)

	permCookie, err := loginLearn(loginURL, username, password)
	checkErr(err)

	learnCookie = permCookie // set global learnCookie

	courseMap := getCoursesLinks()

	for courseTitle, courseURL := range courseMap {
		if *coursePtr != "all" {
			if !strings.HasPrefix(courseTitle, *coursePtr) {
				continue
			}
		}

		cYel.Printf("Parsing %s\n", courseTitle)
		//if courseURL != "https://learn.canterbury.ac.nz/course/view.php?id=5552" {
		//	continue
		//}

		dlFiles := parseCoursePage(courseURL, db)
		for _, fileInfo := range dlFiles {
			courseChapter := fileInfo.chapter
			fileName := fileInfo.name
			fileURL := fileInfo.url
			filetype := fileInfo.filetype
			fileFullname := fileName + "." + filetype
			fullPath := filepath.Join(downloadDir,
				courseTitle,
				courseChapter)
			os.MkdirAll(fullPath, 0755)
			downloaded, err := get(fileURL, filepath.Join(fullPath, fileFullname), db)
			checkErr(err)

			for !downloaded {
				downloaded, err = get(fileURL, filepath.Join(fullPath, fileFullname), db)
				checkErr(err)
			}
		}
	}

	permCookie, err = loginEzproxy(ezproxyURL, username, password) // set global exproxyCookie
	checkErr(err)

	ezproxyCookie = permCookie
	guessedCoursesNames := guessCoursesName(courseMap)

	pastPapers := getCoursesPastPapers(guessedCoursesNames, db)
	for courseName, paperURLs := range pastPapers {
		for _, paper := range paperURLs {
			filenameSplit := strings.Split(paper, "/")
			dirName := filepath.Join(downloadDir,
				"Past Papers",
				courseName)
			os.MkdirAll(dirName, 0755)
			fullName := filepath.Join(dirName,
				filenameSplit[len(filenameSplit)-2]+
					filenameSplit[len(filenameSplit)-1])
			downloaded, err := get(paper, fullName, db)
			checkErr(err)
			for !downloaded {
				downloaded, err = get(paper, fullName, db)
				checkErr(err)
			}
		}
	}

	cYel.Printf("I have stored the files here:\n")
	cRed.Printf(" %s\n", downloadDir)
	waitInputExit()
}
