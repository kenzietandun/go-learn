GO = /usr/local/go/bin/go

deploy: #deploy website
	./build.sh

run:
	./bins/linux/go-learn

linux: # Linux
	$(GO) build -ldflags="-s -w" -o bins/linux/go-learn gitlab.com/kenzietandun/go-learn

windows: # Windows
	env CC=/usr/bin/x86_64-w64-mingw32-gcc-win32 GOOS=windows GOARCH=amd64 $(GO) build -o bins/windows/go-learn.exe *.go 

mac: # MacOS
	env CC=o64-clang GOOS=darwin $(GO) build -o bins/mac/go-learn *.go

send: # 
	rsync -Prv bins/* saltm:/var/www/go-learn/
	rm -rf bins/*

all: windows linux mac deploy send

exceptlinux: windows mac deploy send

onlylinux: linux deploy send
