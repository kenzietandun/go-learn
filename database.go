package main

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func initDatabase(dbFile string) *sql.DB {
	if _, err := os.Stat(dbFile); os.IsNotExist(err) {
		emptyFile, err := os.Create(dbFile)
		checkErr(err)
		emptyFile.Close()
	}
	database, err := sql.Open("sqlite3", dbFile)
	checkErr(err)

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS history (url TEXT PRIMARY KEY, filename TEXT)")
	statement.Exec()

	return database
}
