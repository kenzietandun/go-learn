# Go-Learn

[Website](https://go-learn.zies.net)

Program written in Golang to download resources from UC's Learn page

### Why 

<del>Because I can't be bothered.</del>

Doing repetitive things is best handed to computers.

### Install

Get it from the [Website](https://go-learn.zies.net)

### Usage

Run the program as usual according to your OS. 

If you are using Mac or Linux, run it from the terminal like so

```
./go-learn
```

To find resources on certain courses, use the `--course` flag like so

```
./go-learn --course COSC121
```
