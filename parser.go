package main

import (
	"database/sql"
	"fmt"
	_ "fmt"
	"io"
	"net/http"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/fatih/color"
	_ "github.com/mattn/go-sqlite3"
)

func getHiddenLoginToken(resp *http.Response) string {
	doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
	checkErr(err)
	loginToken := ""
	doc.Find("input").Each(func(i int, s *goquery.Selection) {
		inputName, exists := s.Attr("name")
		if exists && inputName == "logintoken" {
			loginToken, _ = s.Attr("value")
		}
	})

	return loginToken
}

func guessCoursesName(coursesMap map[string]string) []string {

	re := regexp.MustCompile(`^([A-Z]{4,})([\-A-Z]+)?(\d{3,})([\-\d\w]+)?\s`)
	var guessedCoursesNames []string
	for course := range coursesMap {
		match := re.FindAllStringSubmatch(course, -1)[0]
		guessedName := match[1] + match[3]
		guessedCoursesNames = append(guessedCoursesNames, guessedName)
	}

	return guessedCoursesNames
}

func getCoursesPastPapers(courses []string, db *sql.DB) map[string][]string {
	requestURL := "http://library.canterbury.ac.nz/exams/index.php?year=&course="

	insStatement, err := db.Prepare("INSERT OR IGNORE INTO history (url, filename) VALUES (?, ?)")
	checkErr(err)

	pastPapers := make(map[string][]string)

	c := color.New(color.FgYellow)
	for _, course := range courses {
		c.Printf("Searching past papers for %s\n", course)
		req, err := http.NewRequest("GET", requestURL+course, nil)
		setReqHeaders(req, ezproxyCookie, ezproxyURL)

		resp, err := client.Do(req)
		checkErr(err)

		defer resp.Body.Close()

		doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
		checkErr(err)

		doc.Find("div.col-lg-12 a").Each(func(i int, s *goquery.Selection) {
			linkedFile := s.Text()
			url, exists := s.Attr("href")
			if isInDatabase(url, db) {
				return
			}

			if exists && strings.HasSuffix(url, ".pdf") {
				pastPapers[course] = append(pastPapers[course], url)
			}

			_, err = insStatement.Exec(url, linkedFile)
			checkErr(err)
		})
	}

	return pastPapers
}

func getURLPastPaperProxy(htmlPage io.Reader) string {
	doc, err := goquery.NewDocumentFromReader(htmlPage)
	checkErr(err)

	foundURL := ""
	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		url, exists := s.Attr("href")
		if exists && strings.HasSuffix(url, ".pdf") {
			foundURL = url
		}
	})

	return foundURL
}

func getCoursesLinks() map[string]string {
	//client := setupHTTPClient()
	req, err := http.NewRequest("GET", "https://learn.canterbury.ac.nz/my/", nil)
	setReqHeaders(req, learnCookie, loginURL)

	resp, err := client.Do(req)
	checkErr(err)

	defer resp.Body.Close()

	courseMap := make(map[string]string)

	doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
	checkErr(err)
	doc.Find("a.dropdown-item").Each(func(i int, s *goquery.Selection) {
		courseTitle := s.Text()
		if courseTitle == "LEARN" {
			return
		}

		// Contacts page having problems, temporary fix?
		if strings.HasPrefix(courseTitle, "ENGR200") {
			return
		}

		url, exists := s.Attr("href")
		if exists {
			matched, err := regexp.MatchString(`^[A-Z]{4,}\S`, courseTitle)
			checkErr(err)
			if matched {
				courseMap[courseTitle] = url
			}
		}
	})

	return courseMap
}

type downloadableFiles struct {
	chapter  string
	name     string
	url      string
	filetype string
}

func parseCoursePage(courseURL string, db *sql.DB) []downloadableFiles {
	//client := setupHTTPClient()

	req, err := http.NewRequest("GET", courseURL, nil)
	setReqHeaders(req, learnCookie, rootURL)

	resp, err := client.Do(req)
	checkErr(err)

	defer resp.Body.Close()

	courseChapterMap := make(map[string]string)

	doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
	checkErr(err)

	doc.Find("a.list-group-item.list-group-item-action").Each(func(i int, s *goquery.Selection) {
		courseChapter := s.Text()
		courseChapter = strings.TrimSpace(courseChapter)
		url, exists := s.Attr("href")
		tag, isCoursePage := s.Attr("data-parent-key")
		if isCoursePage {
			if tag != "localboostnavigationcoursesections" {
				return
			}
		}

		if exists {
			courseChapterMap[courseChapter] = url
		}
	})

	courseChapterMap["MainPage"] = courseURL + "&home=1"

	dlFiles := parseCourseChapters(courseChapterMap, db)
	return dlFiles
}

//this function takes a map of courseChapter Name->url
//iterates through the map
//and finds all the downloadable files in the courseChapter
//
//Returns list of string of downloadable files url
func parseCourseChapters(courseChapterMap map[string]string, db *sql.DB) []downloadableFiles {
	//client := setupHTTPClient()

	var dlFiles []downloadableFiles

	cBlue := color.New(color.FgBlue)
	cCyan := color.New(color.FgCyan)

	for chapter, url := range courseChapterMap {
		if chapter == "Contacts" {
			continue
		}
		cBlue.Printf("-- Processing %s\n", chapter)
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			fmt.Println("Unable to process URL: " + url)
			continue
		}
		setReqHeaders(req, learnCookie, rootURL)

		resp, err := client.Do(req)
		if err != nil {
			fmt.Println("Unable to process URL: " + url)
			continue
		}

		// insStatement, err := db.Prepare("INSERT OR IGNORE INTO history (url, filename) VALUES (?, ?)")
		// checkErr(err)

		doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
		doc.Find("section#region-main a").Each(func(i int, s *goquery.Selection) {
			var replacer = strings.NewReplacer("/", "-")
			linkedFile := s.Text()
			linkedFile = replacer.Replace(linkedFile)

			if strings.HasSuffix(linkedFile, " File") {
				tmp := strings.Split(linkedFile, " ")
				linkedFile = strings.Join(tmp[0:len(tmp)-1], " ")
			}
			url, exists := s.Attr("href")
			if exists {
				if isInBlacklist(url) {
					return
				}

				if !isValidURL(url) {
					return
				}

				if isInDatabase(url, db) {
					return
				}

				trueURL, filetype := getURLAndFiletype(url)
				if filetype != "unknown" {
					dlFiles = append(dlFiles, downloadableFiles{
						chapter:  chapter,
						name:     linkedFile,
						url:      trueURL,
						filetype: filetype})
					cCyan.Printf("---- Found %s - Type %s\n", linkedFile, filetype)
				}

				// _, err = insStatement.Exec(url, filetype)
				// checkErr(err)
			}
		})
	}

	return dlFiles
}

func isValidFiletype(url string) (bool, string) {
	validFiletypes := []string{
		"pdf", "csv", "doc", "docx", "ipynb", "sql",
		"xls", "xlsx", "zip",
	}

	for i := 0; i < len(validFiletypes); i++ {
		if strings.HasSuffix(url, validFiletypes[i]) ||
			strings.HasSuffix(url, validFiletypes[i]+"?forcedownload=1") {
			return true, validFiletypes[i]
		}
	}

	return false, "unknown"
}

func isInBlacklist(url string) bool {
	return strings.HasPrefix(url, "https://www.youtube.com") ||
		strings.HasPrefix(url, "https://www.dropbox.com")

}

//checks if url is valid
//some urls starts with "mailto:" which crashes this script
func isValidURL(url string) bool {
	return strings.HasPrefix(url, "http")
}

func isInDatabase(url string, db *sql.DB) bool {
	queryStatement, err := db.Prepare("SELECT url FROM history")
	checkErr(err)

	rows, err := queryStatement.Query()
	checkErr(err)

	foundURL := ""
	found := false
	for rows.Next() {
		rows.Scan(&foundURL)
		if foundURL == url {
			found = true
			break
		}
	}

	defer rows.Close()

	return found
}

func getURLAndFiletype(url string) (string, string) {
	if strings.HasSuffix(url, ".pdf") {
		return url, "pdf"
	}

	if strings.HasPrefix(url, "https://learn.canterbury.ac.nz/mod/folder/") {
		return url, "zip"
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return url, "unknown"
	}

	setReqHeaders(req, learnCookie, rootURL)

	resp, err := client.Do(req)
	checkErr(err)
	defer resp.Body.Close()

	redirectURL := resp.Header.Get("Location")
	if redirectURL != "" {
		if strings.HasSuffix(redirectURL, ".pdf") ||
			strings.HasSuffix(redirectURL, ".pdf?forcedownload=1") {
			return redirectURL, "pdf"
		} else if isGoogleDriveLink(url) {
			return parseGoogleDocs(url)
		} else {
			validFiletype, filetype := isValidFiletype(redirectURL)
			if validFiletype {
				return redirectURL, filetype
			}
		}
	}

	doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
	checkErr(err)
	filetype := "unknown"
	foundURL := ""

	doc.Find("section#region-main a").Each(func(i int, s *goquery.Selection) {
		url, exists := s.Attr("href")
		if exists && foundURL == "" {
			if isGoogleDriveLink(url) {
				foundURL, filetype = parseGoogleDocs(url)
			}
		}
	})

	if foundURL != "" {
		return foundURL, filetype
	}

	doc.Find("div.resourcecontent.resourcepdf object").Each(func(i int, s *goquery.Selection) {
		url, exists := s.Attr("data")
		if exists {
			if strings.HasSuffix(url, ".pdf") {
				filetype = "pdf"
			}
			foundURL = url
		}
	})

	return foundURL, filetype
}

func parseFolderLink(url string) (string, string) {
	req, err := http.NewRequest("GET", url, nil)
	checkErr(err)
	setReqHeaders(req, "", "")

	resp, err := client.Do(req)
	checkErr(err)
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(io.Reader(resp.Body))
	checkErr(err)

	id := ""
	sessKey := ""

	doc.Find("div input").Each(func(i int, s *goquery.Selection) {
		name, _ := s.Attr("name")
		if name == "id" {
			id, _ = s.Attr("value")
		}

		if name == "sesskey" {
			sessKey, _ = s.Attr("value")
		}
	})

	return id, sessKey
}

func isGoogleDriveLink(url string) bool {
	return strings.HasPrefix(url, "https://drive.google.com") ||
		strings.HasPrefix(url, "https://docs.google.com/presentation")
}

func parseGoogleDocs(url string) (string, string) {

	redirectURL := ""

	if strings.HasPrefix(url, "https://drive.google.com/") {
		req, err := http.NewRequest("GET", url, nil)
		checkErr(err)
		setReqHeaders(req, "", "")

		resp, err := client.Do(req)
		checkErr(err)
		defer resp.Body.Close()

		redirectURL = resp.Header.Get("location")
	} else {
		redirectURL = url
	}

	re := regexp.MustCompile(`(https\:\/\/[\w\.]+\/presentation\/d\/([\w\-]+))(\/.*)?`)
	match := re.FindAllStringSubmatch(redirectURL, -1)[0]
	if len(match) == 0 {
		return "", ""
	}

	foundURL := "https://docs.google.com/presentation/d/" + match[2] + "/export/pdf"

	return foundURL, "pdf"

}
