#!/bin/bash
cp templateindex.html index.html
VER="$(grep VERSION main.go | head -n 1 | cut -d '"' -f2)"

sed -i "s/\%VERSION\%/${VER}/g" index.html
touch latest.txt
echo "${VER}" > latest.txt
rsync -P index.html guide.html latest.txt saltm:/var/www/go-learn
