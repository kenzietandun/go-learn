package main

import (
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"time"

	"golang.org/x/net/publicsuffix"
)

func setupHTTPClient() (*http.Client, error) {
	option := cookiejar.Options{
		// mysterious thing needed to setup cookiejar
		PublicSuffixList: publicsuffix.List,
	}

	jar, err := cookiejar.New(&option)
	if err != nil {
		return nil, err
	}

	tr := &http.Transport{
		MaxIdleConns:        20,
		MaxIdleConnsPerHost: 20,
		TLSHandshakeTimeout: 10 * time.Second,
	}

	httpClient := &http.Client{
		Timeout: time.Second * 60,
		// dont redirect automatically
		// useful for obtaining cookies when loggin' in
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Jar:       jar,
		Transport: tr}

	return httpClient, nil
}

func setReqHeaders(req *http.Request, cookie string,
	referer string) {
	req.Header.Set("Host", "learn.canterbury.ac.nz")
	req.Header.Set("Referer", referer)
	if strings.HasPrefix(cookie, "ezproxy") {
		req.Header.Set("Host", "library.canterbury.ac.nz.ezproxy.canterbury.ac.nz")
	}
	req.Header.Set("User-Agent",
		"Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0")
	req.Header.Set("Accept",
		"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")
	req.Header.Set("Accept-Encoding", "gzip, deflate, br")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Cookie", cookie)
	req.Header.Set("DNT", "1")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Upgrade-Insecure-Requests", "1 ")
}

func setupForm(targetURL string, username string, password string, logintoken string) url.Values {
	if targetURL == loginURL {
		return url.Values{
			"anchor":     {"#login"},
			"username":   {username},
			"password":   {password},
			"logintoken": {logintoken},
		}
	}
	return url.Values{
		"user": {username},
		"pass": {password},
	}
}

func getInitialCookie(url string, client *http.Client) (string, string, error) {
	if moodleCookie != "" {
		return moodleCookie, "", nil
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", "", err
	}

	resp, err := client.Do(req)
	if err != nil {
		return "", "", err
	}

	loginToken := getHiddenLoginToken(resp)

	defer resp.Body.Close()

	tempCookie := resp.Header["Set-Cookie"][0]
	return tempCookie, loginToken, nil
}

func getPermanentCookie(client *http.Client, form url.Values,
	initialCookie string, url string) (string, error) {

	stringReader := strings.NewReader(form.Encode())
	req, _ := http.NewRequest("POST", url,
		stringReader)
	setReqHeaders(req, initialCookie, url)

	client.Do(req)

	return "", nil
}

func loginLearn(url string, username string, password string) (string, error) {

	tempCookie, loginToken, err := getInitialCookie(url, client)

	if err != nil {
		return "", err
	}

	form := setupForm(url, username, password, loginToken)
	permCookie, err := getPermanentCookie(client, form, tempCookie, url)

	if err != nil {
		return "", err
	}

	return permCookie, nil
}

func loginEzproxy(url string, username string, password string) (string, error) {

	form := setupForm(url, username, password, "")
	permCookie, err := getPermanentCookie(client, form, "", url)
	if err != nil {
		return "", err
	}

	return permCookie, nil
}
