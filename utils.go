package main

import (
	"bufio"
	"fmt"
	"github.com/fatih/color"
	"os"
)

// PrintASCII prints a kewl logo :D
func PrintASCII() {
	c := color.New(color.FgCyan)
	c.Println(`
                    _                       
   __ _  ___       | | ___  __ _ _ __ _ __  
  / _  |/ _ \ _____| |/ _ \/ _  | '__| '_ \ 
 | (_| | (_) |_____| |  __/ (_| | |  | | | |
  \__, |\___/      |_|\___|\__,_|_|  |_| |_|
  |___/                                    `)
}

func checkErr(err error) {
	c := color.New(color.FgRed)

	if err != nil {
		c.Printf("[ERR] %v\n", err)
		os.Exit(1)

		waitInputExit()
	}
}

func waitInputExit() {
	fmt.Println("Press <Enter> to close the program")
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	if input != "" {
		os.Exit(0)
	}
}

func PromptUpdate() {
	fmt.Println("[INFO] There's a new version of go-Learn.")
	fmt.Println("[INFO] To update, download the latest version from https://go-learn.zies.net and delete the old one")
	fmt.Println()
	fmt.Println()

}
