package main

import (
	"bytes"
	"database/sql"
	_ "fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	_ "time"

	_ "github.com/mattn/go-sqlite3"

	"github.com/fatih/color"
)

func get(linkURL string, destination string, db *sql.DB) (bool, error) {
	// check if file exists
	if _, err := os.Stat(destination); err == nil {
		return true, nil
	}
	filename := filepath.Base(destination)
	c := color.New(color.FgGreen)
	//client := setupHTTPClient()

	id := ""
	sessKey := ""
	req, err := http.NewRequest("GET", linkURL, nil)
	var resp *http.Response
	checkErr(err)

	if strings.HasPrefix(linkURL, "https://learn.canterbury.ac.nz/mod/folder/") {
		id, sessKey = parseFolderLink(linkURL)
		data := url.Values{}
		data.Add("id", id)
		data.Set("sesskey", sessKey)
		req, err = http.NewRequest("POST",
			"https://learn.canterbury.ac.nz/mod/folder/download_folder.php",
			bytes.NewBufferString(data.Encode()))
		setReqHeaders(req, learnCookie, rootURL)
		resp, err = client.Do(req)
		checkErr(err)
		defer resp.Body.Close()
	} else if strings.HasPrefix(linkURL, "https://docs.google.com/presentation") {
		resp, err = client.Do(req)
		checkErr(err)
		defer resp.Body.Close()
	} else if strings.HasPrefix(linkURL, "http://ezproxy.canterbury.ac.nz/") {
		setReqHeaders(req, ezproxyCookie, ezproxyURL)
		resp, err = client.Do(req)
		checkErr(err)
		defer resp.Body.Close()

		byteBody, _ := ioutil.ReadAll(resp.Body)
		pdfURL := getURLPastPaperProxy(bytes.NewReader(byteBody))

		req, err = http.NewRequest("GET", pdfURL, nil)
		setReqHeaders(req, ezproxyCookie, ezproxyURL)
		resp, err = client.Do(req)
		checkErr(err)

		defer resp.Body.Close()
	} else {
		setReqHeaders(req, learnCookie, rootURL)
		resp, err = client.Do(req)
		checkErr(err)
		defer resp.Body.Close()
	}

	c.Printf("++++ Created %s\r", filename)
	f, err := os.Create(destination)
	checkErr(err)

	defer f.Close()

	_, err = io.Copy(f, resp.Body)
	checkErr(err)

	c.Printf("++++ Downloaded %s\n", filename)
	//time.Sleep(1 * time.Second)

	insStatement, err := db.Prepare("INSERT OR IGNORE INTO history (url, filename) VALUES (?, ?)")
	checkErr(err)
	_, err = insStatement.Exec(linkURL, filename)
	checkErr(err)

	fi, err := os.Stat(destination)
	if err != nil {
		return false, err
	}

	fiSize := fi.Size()
	if fiSize < 1000 { // 1 kilobytes
		return false, nil
	}

	return true, nil
}
